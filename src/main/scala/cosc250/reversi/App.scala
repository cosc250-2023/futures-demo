package cosc250.reversi

import scalafx.Includes._
import scalafx.application.JFXApp3
import scalafx.scene.{Scene, Group}
import scalafx.scene.paint.Color
import scalafx.scene.shape.*
import scalafx.scene.layout.*
import scalafx.scene.control.*
import scalafx.geometry.*
import scalafx.collections.*
import scalafx.animation.*
import scalafx.beans.property.*
import javafx.application.Platform

import java.util.{TimerTask, Timer}


object App extends JFXApp3 {

    override def start() = {

        val r = Rectangle(200, 200)
        val b = Button("Click me")
        var hue = 1
        var rot = 0d

        stage = new JFXApp3.PrimaryStage {
            title.value = "Reversi"
            width = 480
            height = 600
            scene = new Scene {
                content = new VBox(
                    r, b
                )                    
            }
        }
        stage.setOnCloseRequest((_) => System.exit(0))

        val timerTask = new TimerTask { 
            override def run():Unit = 
                hue = (hue + 1) % 255
                rot = (rot + 3) % 360

                Platform.runLater { () =>
                    r.fill = Color.hsb(hue, 1, 1)
                    r.rotate = rot
                }
        }
        val timer = new Timer()
        timer.schedule(timerTask, 1000/60, 1000/60);

        b.setOnAction((_) => 
            new Thread(() => {
                try {
                    Thread.sleep(10000);
                } catch  {
                    case _ => // ignore
                }
            }).start();
            
        )
    }

}
