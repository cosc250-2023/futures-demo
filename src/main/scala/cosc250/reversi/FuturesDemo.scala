package cosc250.reversi

import scala.util.*
import scala.concurrent.* 
import scala.concurrent.ExecutionContext.Implicits.global



@main def futuresDemo() = {

    val promise = Promise[Int]()
    val future = promise.future

    println(s"Before the future is completed. Future is completed ${future.isCompleted}")
    future.onComplete { case Success(v) => println(s"Future completed with value $v") }
    promise.success(2)
    //println(s"After the future is completed. Future is completed ${future.isCompleted}")

}
