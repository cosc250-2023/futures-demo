package cosc250.reversi

import akka.actor.ActorSystem
import akka.stream._
import play.api.libs.ws._
import play.api.libs.ws.ahc.StandaloneAhcWSClient
import scala.concurrent.ExecutionContext.Implicits.global


import scala.util.Random

import scala.concurrent.Future

@main def wsmain(): Unit = {
    // A little boilerplate - this is needed by the web client we're using
  given actorSystem:ActorSystem = ActorSystem()
  given materializer:Materializer = SystemMaterializer(actorSystem).materializer

  // Now we can create our web client and make a request
  val wsClient = StandaloneAhcWSClient()

  def zen():Future[String] =
    val randomStr = Random.nextString(4) // Makes sure we don't get a cached reply
    wsClient.url(s"https://api.github.com/zen?$randomStr").get().map(_.body)

  def nZens(n:Int):Future[Seq[String]] = {
    val futures:Seq[Future[String]] = for
      i <- 1 to n
    yield zen()

    Future.sequence(futures)
  }

  val f = for
    first <- zen()
    second <- zen()
  yield s"$first, and then $second"

  // Print this statement immediately
  println("This prints immediately")
}